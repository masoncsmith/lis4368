# LIS 4368 - Advanced Web Applications Development

## Mason Smith

### Assignment #4 Requirements:

* LIS4368 Portal (Main Page)
* Screenshot of failed server-side validation
* Screenshot of passed server-side validation

* Link to [local lis4368 web app](http://localhost:9999/lis4368/customerform.jsp?assign_num=a4)

#### Assignment Screenshots:

*Screenshot of failed server-side validation*:

![Failed Validation](img/failed_validation.png)

*Screenshot of passed server-side validation*:

![Passed Validation](img/passed_validation.png)
