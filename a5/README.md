# LIS 4368 - Advanced Web Applications Development

## Mason Smith

### Assignment #5 Requirements:

* LIS4368 Portal (Main Page)
* Screenshot of a valid user entry form
* Screenshot of passed validation using thanks.jsp
* Screenshot of the associated database entry

* Link to [local lis4368 web app](http://localhost:9999/lis4368/customerform.jsp?assign_num=a5)

#### Assignment Screenshots:

*Valid User Entry Form*:

![Valid User Entry Form](img/valid_user_entry.png)

*Passed Validation (thanks.jsp)*:

![Passed Validation](img/passed_validation.png)

*Associated Database Entry*:

![Associated Database Entry](img/db_data_entry.png)
