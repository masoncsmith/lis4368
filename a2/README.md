# LIS 4368 - Advanced Web Applications Development

## Mason Smith

### Assignment #2 Requirements:

- Download and install MySQL
- Create a new user
- Develop a web app
- Deploy using *@WebServlet*

#### Assignment Links:

[localhost:9999/hello](http://localhost:9999/hello "")

[localhost:9999/hello/HelloHome.html](http://localhost:9999/hello/HelloHome.html "")

[localhost:9999/hello/sayhello](http://localhost:9999/hello/sayhello "")

[localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html "")

[localhost:9999/hello/sayhi](http://localhost:9999/hello/sayhi "")

#### Assignment Screenshots:

*Screenshot of querybook.html:*

![querybook.html](img/querybook.png)

*Screenshot of querybook.html results:*

![querybook_results.png](img/querybook_results.png)
