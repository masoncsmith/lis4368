# LIS 4368 - Advanced Web Applications Development

## Mason Smith

### Project #1 Requirements:

* Screenshots of:

 - LIS4368 Portal (Main Page)
 - P1 Failed Validation
 - P1 Passed Validation

* Link to [local lis4368 web app](http://localhost:9999/lis4368/p1/index.jsp)

#### Assignment Screenshots:

*LIS4368 Portal (Main Page)*:

![LIS4368 Portal (Main Page)](img/lis_main.png)

*Screenshot of failed validation*:

![Failed Validation](img/failed_validation.png)

*Screenshot of passed validation*:

![Passed Validation](img/passed_validation.png)
