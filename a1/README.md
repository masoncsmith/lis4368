# LIS 4368 - Advanced Web Applications Development

## Mason Smith

### Assignment #1 Requirements:

- Install JDK
- Install Tomcat
- Git commands w/ descriptions
- Bitbucket Tutorial links

#### Git commands w/short descriptions:

1. git init - creates a Git repo
2. git status - displays the status of your git staging area
3. git add - adds files to your staging area
4. git commit - commits changes in your staging area to your local repo
5. git push - pushes your local repo commits to your remote repo
6. git pull - pulls changes from a remote repo
7. git clone - clones an existing repository

#### Assignment Screenshots:

*Screenshot of running java Hello:*

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Tomcat running:* [http://localhost:9999](http://localhost:9999)

![Tomcat Installation Screenshot](img/tomcat.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/masoncsmith/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/masoncsmith/myteamquotes/ "My Team Quotes Tutorial")
