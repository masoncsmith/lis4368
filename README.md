# LIS 4368 - Advanced Web Applications Development

## Mason Smith

### Course Assignment Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")

    - Install JDK
    - Install Tomcat
    - Git commands w/ descriptions
    - Bitbucket Tutorial links

2. [A2 README.md](a2/README.md "My A2 README.md file")

    - Download and install MySQL
    - Create a new user
    - Develop a web app
    - Deploy using @WebServlet

3. [A3 README.md](a3/README.md "My A3 README.md file")

    - Screenshot of ERD
    - Link to a3.mwb
    - Link to a3.sql

4. [P1 README.md](p1/README.md "My P1 README.md file")

    - Screenshot of LIS4368 Portal (Main Page)
    - Screenshot of failed client-side validation
    - Screenshot of passed client-side validation
    - Link to [local lis4368 web app](http://localhost:9999/lis4368/p1/index.jsp)

5. [A4 README.md](a4/README.md "My A4 README.md file")

    - Screenshot of failed server-side validation
    - Screenshot of passed server-side validation
    - Link to [local lis4368 web app](http://localhost:9999/lis4368/a4/index.jsp)

6. [A5 README.md](a5/README.md "My A5 README.md file")

    - Screenshot of a valid user entry form
    - Screenshot of passed validation using thanks.jsp
    - Screenshot of the associated database entry

7. [P2 README.md](p2/README.md "My P2 README.md file")

	- Add funtionality to show current data
	- Finshed CRUD Functionality
	- Create README file
