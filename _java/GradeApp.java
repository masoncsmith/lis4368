//Mason Smith - March 22, 2017

import java.text.DecimalFormat;
import java.util.Scanner;

public class GradeApp
{
  public static void main(String[] args)
  {
    Scanner input = new Scanner(System.in);
    DecimalFormat df = new DecimalFormat("#.00");

    boolean stopCounting = false;
    double totalGrade = 0;
    double nextGrade = 0;
    double avgGrade = 0.00;
    int count = 0;

    System.out.println("Please enter grades that range from 0 to 100.");
    System.out.println("Grade average and total is rounded to 2 decimal places.");
    System.out.println("Note: Program does *not* check from non-numeric characters.");
    System.out.println("Enter -1 to end program.\n");

    while (!stopCounting) {
      System.out.print("Please enter grades that range from 0 to 100: ");
      nextGrade = input.nextDouble();

      if (nextGrade == -1.0) {
        stopCounting = true;
        continue;   //restart loop
      } else if (nextGrade < 0 || nextGrade > 100)  {
        System.out.println("Invalid entry, not counted.");
        continue;   //restart loop
      }

      totalGrade += nextGrade;  //add grade to total
      count++;        //increment counter
      nextGrade = 0;  //reset grade holder
    }

    avgGrade = (totalGrade / count);  //calculate avg

    System.out.println("\nGrade count:\t" + count);
    System.out.println("Grade total:\t" + df.format(totalGrade));
    System.out.println("Average grade:\t" + df.format(avgGrade));
  }
}
